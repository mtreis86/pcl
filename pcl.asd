#|
  This file is a part of pcl project.
|#

(defsystem "pcl"
  :version "0.0.0"
  :author "Michael Reis"
  :license "public"
  :depends-on ()
  :components
  ((:module "src"
    :components
    ((:file "packages")
     (:file "utils")
     (:file "testing")
     (:file "pathnames"))))
  :description "Practical Common Lisp examples"
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "pcl-test"))))
