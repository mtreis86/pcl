#|
  This file is a part of pcl project.
|#

(defsystem "pcl-test"
  :defsystem-depends-on ("prove-asdf")
  :author ""
  :license ""
  :depends-on ("pcl"
               "prove")
  :components ((:module "tests"
                :components
                ((:test-file "pcl"))))
  :description "Test system for pcl"

  :perform (test-op (op c) (symbol-call :prove-asdf :run-test-system c)))
