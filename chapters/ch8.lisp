;;;;Chapter 8
;;;;From PCL

(in-package :cl-user)

(defpackage :ch8
  (:use :common-lisp))

(defun primep (num)
  (when (> num 1)
    (loop for fac from 2 to (isqrt num) never (zerop (mod num fac)))))

(defun next-prime (num)
  (loop for n from num when (primep n) return n))

(defun print-primes (min max)
  (do ((p (next-prime min) (next-prime (1+ p))))
      ((> p max))
    (format t "~d " p)))

(defmacro do-primes ((num min max) &body body)
  (let ((max-gen (gensym)))
    `(do ((,num (next-prime ,min) (next-prime (1+ ,num)))
          (,max-gen ,max))
         ((> ,num ,max-gen))
       ,@body)))

(defun test ()
  (assert (eql (print-primes 0 19)
               (do-primes (p 0 19)
                 (format t "~d " p)))))

(defmacro once-only ((&rest names) &body body)
  (let ((gensyms (loop for n in names collect (gensym))))
    `(let (,@(loop for g in gensyms collect `(,g (gensym))))
       `(let (,,@(loop for g in gensyms for n in names collect ``(,,g ,,n)))
          ,(let (,@(loop for n in names for g in gensyms collect `(,n ,g)))
             ,@body)))))


(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))
