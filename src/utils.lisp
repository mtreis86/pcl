;;;;Utils
;;;;From PCL

(defmacro with-gensyms ((&rest names) &body body)
  "Insert a let statement for each name utilizing a gensym to avoid symbol clash."
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))

(defmacro once-only ((&rest names) &body body)
  "Insert a let statement for each name utilizing a gensym to avoid symbol clash. Evaluate these only once, and in order."
  (let ((gensyms (loop for n in names collect (gensym))))
    `(let (,@(loop for g in gensyms collect `(,g (gensym))))
       `(let (,,@(loop for g in gensyms for n in names collect ``(,,g ,,n)))
          ,(let (,@(loop for n in names for g in gensyms collect `(,n ,g)))
            ,@body)))))
